document.getElementById('namesortascend').addEventListener('click',sortedAscend);
document.getElementById('namesortdescend').addEventListener('click',sortedDescend);
document.getElementById('datesortascend').addEventListener('click',DateAscend);
document.getElementById('datesortdescend').addEventListener('click',DateDescend);
document.getElementById('search-item').addEventListener('keyup',searchEmployee);
document.getElementById('form').addEventListener('submit',getInput);
document.getElementById('firstName').addEventListener('blur', validateName);
document.getElementById('newPhone').addEventListener('blur', validatePhone);
document.getElementById('newEmail').addEventListener('blur', validateEmail);
// document.getElementById('newId').addEventListener('blur', validateId);

var index=-1;
var data;
function getJson(){
    fetch('customers.json')
      .then(function(res){
          return res.json();
      })
     .then(function(data1){
       
            data=data1; 
            display(data);
           
        })
    .catch(function(err){
        console.log(err);
    });
}
getJson();
console.log(data);

   
  function display(data){

  let output = '';

  data.forEach(function(customer){
    output += `
    <tr>
      <td> ${customer.id}</td>
      <td> ${customer.salutation}</td>
      <td> ${customer.fname}</td>
      <td> ${customer.lname}</td>
      <td> ${customer.email}</td>
      <td> ${customer.gender}</td>
      <td> ${customer.phone}</td>
      <td> ${customer.country}</td>
      <td> ${customer.state}</td>
      <td> ${customer.city}</td>
      <td> ${customer.date}</td>
      <td><button id="btnUpdate" id="update-row" onclick="displayNoneUp(${customer.id})"><i class="fa fa-edit"></i></button
    </td>
      <td> <button onclick="SomeDeleteRowFunction(${customer.id})" i class="fa fa-trash"></button>

    </tr>
  `;
  });
 

  document.querySelector('tbody').innerHTML = output;
}

function displayNoneUp(v){

  document.getElementById('abc').style.display= "block"
   UpdateRowFunction(v); 

  // console.log(del);
 }

 function UpdateRowFunction(v){
  console.log(v);
  
  // console.log(v);
 
 for(i=0;i<data.length;i++){
    if(data[i].id==v)
    {
      index=i;
    }
 }
 var hum=data[index].gender;
 if(hum=='male'){
  document.getElementById("male").checked = true;

 }
 if(hum=='female'){
  document.getElementById("female").checked = true;

 }
 document.getElementById('firstName').value=data[index].fname;
 document.getElementById('lastName').value=data[index].lname;
 document.getElementById('newEmail').value=data[index].email;
 document.getElementById('newCity').value=data[index].city;
 document.getElementById('newPhone').value=data[index].phone;
 }


//name sort
function sortedAscend(){
    console.log(data);
    data.sort((a, b) => {
       let fa = a.fname.toLowerCase(),
       fb = b.fname.toLowerCase();
       if (fa > fb) {
           return 1;
       }
       if (fa < fb) {
       return -1;
   }
   return 0;
});
  
 display(data);
   
}
function sortedDescend(){
    console.log(data);
    data.sort((a, b) => {
       let fa = a.fname.toLowerCase(),
       fb = b.fname.toLowerCase();
       if (fa < fb) {
           return 1;
       }
       if (fa > fb) {
       return -1;
   }
   return 0;
});
  
 display(data);
   
}
//date
function DateAscend(){
  console.log(data);
  data.sort((a, b) => {
    let da = new Date(a.date),
        db = new Date(b.date);
    return da - db;
  });
 display(data);
};

function DateDescend(){
  console.log(data);
  data.sort((a, b) => {
    let da = new Date(a.date),
        db = new Date(b.date);
    return db - da;
  });
  display(data);
}


    //Function To Display Popup
    function div_show() {
    document.getElementById('abc').style.display= "block"
    }
    //Function to Hide Popup
    function div_hide(){
    document.getElementById('abc').style.display = "none";
    }
    //search

    function searchEmployee(e){
      
      console.log(e.target.value);
      var searchAll=data;
      console.log(searchAll);
      let compfirst,complast;
    
      
      var table=document.getElementById('tab_output');
      table.innerHTML='';
      for(var i=0;i<searchAll.length;i++)
      {   
               compfirst=searchAll[i].fname.toLowerCase();
               console.log(compfirst);
               complast=searchAll[i].lname.toLowerCase();
               let len=e.target.value.toLowerCase();
              
            if((compfirst.includes(len))||(complast.includes(len)))
            {
              
              console.log('Matches');
              var row=`<tr>
              <td>${searchAll[i].id}</td>
              <td>${searchAll[i].salutation}</td>
              <td>${searchAll[i].fname}</td>
              <td>${searchAll[i].lname}</td>
              <td>${searchAll[i].email}</td>
              <td>${searchAll[i].gender}</td>
              <td>${searchAll[i].phone}</td>
              <td>${searchAll[i].city}</td>
              <td>${searchAll[i].state}</td>
              <td>${searchAll[i].country}</td>
              <td>${searchAll[i].date}</td>
              <td> <button i class="fa fa-edit"></button></td>
              <td> <button i class="fa fa-trash"></button></td>
               </tr>`
              table.innerHTML+=row; 
  console.log(data[i]);
            }
            else{
              console.log('NOT Matches');
             
            }
          }
       }
//state and country
      var stat;
       function myCountry(){
        fetch('state.json')
        .then(function(res){
            return res.json();
        })
          
        .then(function(data){
              let output_sal='';
              console.log(data);
              var htr=document.getElementById("mySelect").value;
              console.log(htr);
              data.forEach(post => {
                  if(post.id==htr){
                    output_sal+=`
                  
                       <label>state</label>
                    <select id="myState" >
                      <option value="${post.state1}">${post.state1}</option>
                      <option value="${post.state2}">${post.state2}</option>
                      <option value="${post.state3}">${post.state3}</option>
                      <option value="${post.state4}">${post.state4}</option>
                      <option value="${post.state5}">${post.state5}</option>
                      <option value="${post.state6}">${post.state6}</option>
                      <option value="${post.state7}">${post.state7}</option>
                      </select>
                    `;
                    
                     
                  }
                 
          });
          document.querySelector('#myState').innerHTML=output_sal;  
          document.getElementById("myState").addEventListener('change',stateSelection);
          console.log(stat);
          
      })
      .catch(function(err){
              console.log(err);
          });
          
    }
    function stateSelection(e) {
      {
        stat=(e.target.value);
    
      }}


 var count;
 var sal;

    function getInput(e)
  {  

    var table=document.getElementById('tab_output');
    var fname=document.getElementById('firstName').value;
    myCount();
    mySal();
    let sex;
    if (document.getElementById('male').checked) {
        sex = document.getElementById('male').value;
        console.log(sex);
      }
      else if (document.getElementById('female').checked) {
        sex = document.getElementById('female').value;
        console.log(sex);
      }

    console.log(stat);
    console.log(count);
    var dateTime=new Date();
      var date = new Date(dateTime.getTime());
        date.setHours(0, 0, 0, 0);
      var day=date.getDate();
      var month=date.getMonth();
      var year=date.getFullYear();
      var shortDate = month + "/" + day + "/" + year;
      var stringDate=shortDate.toString();
      var id=document.getElementById('newId').value;
      var lname=document.getElementById('lastName').value;
      var email=document.getElementById('newEmail').value;
      
      var phone=document.getElementById('newPhone').value;
      
      var city=document.getElementById('newCity').value;
   if (index==-1)
   
   { data.push({"fname":fname,"date":stringDate,"lname":lname,"country":count,"state":stat,"gender":sex,"email":email,"phone":phone,"city":city,"salutation":sal,"id":id});
    } 
    else{
      data[index].fname=fname;
        data[index].lname=lname;
        data[index].city=city;
        data[index].phone=phone;
        data[index].email=email;
        data[index].country=count;
        data[index].state=stat;
        data[index].gender=sex;
        data[index].salutation=sal;
        index=-1;

    }
   table.innerHTML='';
    display(data);
    console.log(data);

    e.preventDefault();
    
}
function myCount(){
  count=document.getElementById("mySelect").value;
  console.log(count);
}
function mySal(){
  sal=document.getElementById("salutation").value;
  console.log(sal);
}



function closeForm() {
  document.getElementById("abc").style.display = "none";
}
// delete
function SomeDeleteRowFunction(v) {
   
  console.log(v);
  if(confirm('Are you sure to delete'))
  {

      data.splice(v,1);
      console.log(data);
      var table=document.getElementById('tab_output');
      table.innerHTML='';
      display(data);
      alert('Successfully Deleted');
 }
}

//edit
function SomeEditRowFunction() {
  
 
}

function validateEmail() {
  const email = document.getElementById('newEmail');
  const re = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;

  if(!re.test(email.value)){
    email.classList.add('is-invalid');
  } else {
    email.classList.remove('is-invalid');
  }
}

function validatePhone() {
  const phone = document.getElementById('newPhone');
  const re = /^\(?\d{3}\)?[-. ]?\d{3}[-. ]?\d{4}$/;

  if(!re.test(phone.value)){
    phone.classList.add('is-invalid');
  } else {
    phone.classList.remove('is-invalid');
  }
}
function validateName() {
  const name = document.getElementById('firstName');
  const re = /^[a-zA-Z]{2,10}$/;

  if(!re.test(name.value)){
    name.classList.add('is-invalid');
  } else {
    name.classList.remove('is-invalid');
  }
}

function checkEmpty() {
  if (document.getElementById('id').value == "" || document.getElementById('fname').value == "" || document.getElementById('lname').value == "" || document.getElementById('email').value == "" || document.getElementById('gender').value == "" || document.getElementById('phone').value == "" || document.getElementById('country').value == "" || document.getElementById('state').value == "" || document.getElementById('city').value == "" || document.getElementById('date').value == "") {
  alert
  } else 
  return(true)
  }
  
  // function validateId(){
  //   const id=document.getElementById('newId');
  //   const re =/^[0-9]{1,6}$/;
  
  // }
  // if(!re.test(id.value)){
  //   alert("please Enter a valid Id having digits");
  //   id.value='';
  // } data.forEach(function(){
  //   if(id.value==data.id){
  //     alert("this id is already used");
  //   id.value='';
  //   }
  // })

//
/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}



